<br />
  <h3 align="center">Back End Of React Website</h3>

This is the final project for FS 1030

### Installation
1. Create .env file with
  
* PORT=4000
* JWT_SECRET= put your own JWT secret here 
  

  Default admin is test@test.com
  Password: 12345678

  
2. Install The NPM Packages
   ```sh
   npm install
   ```
   
    
3. Start the API App
   ```sh
   npm start
   ```