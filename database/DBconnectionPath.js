const mysql = require("mysql");

const connection = mysql.createConnection({
  host: "localhost",
  user: "portfolioDB",
  password: "12345678",
  database: "davesportfolio",
});

connection.connect(function (err){
 if (err){
     console.log(err);
 } else{
     console.log("MySQL database is connected");
 }
});

module.exports = connection;
