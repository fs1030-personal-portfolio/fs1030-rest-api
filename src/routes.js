import express from 'express' ;
import jwt from 'jsonwebtoken' ;
import jwtVerify from './middleware/jwtVerify.js' ;
import dotenv from 'dotenv' ;
dotenv.config();
import { verifyHash,createHash, } from './Encrupter.js' ;
import {validateUser } from './middleware/validation.js' ;


//setup Data path
const db = require("../database/DBconnectionPath");

//setup Express Router
const router = express.Router() ;


//** GET BLOG**//
router.get("/blog/entries", function (req, res) {
    db.query(
      "SELECT * FROM blog ORDER BY CreationDate DESC",
      function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
      }
    );
});

//** POST BLOG**//
router.post('/blog/entries', async (req, res, next) => {
    db.query(
        "INSERT INTO blog (Title, subject, content, VideoID, username,imageLink,CreationDate) VALUES (?, ?, ?, ?, ?, ?, ?)",
        [req.body.Title, req.body.subject, req.body.content, req.body.VideoID, req.body.username, req.body.imageLink, req.body.CreationDate],
        function (error, results, fields) {
          if (error) throw error;
          return res.status(201).send(results);
        }
      );
    });

//** DELETE BLOG**//
router.delete('/blog/entries/:id', async (req, res, next) => {
  db.query(`DELETE FROM blog WHERE blogID=${req.params.id}`, function (error, results) {
    if (error) throw error
    return res.status(200).send(results)
})
});


//** PATCH BLOG**//
router.patch("/blog/entries/:id", jwtVerify, async (req, res, next) => {
  const {Title, subject, content, VideoID,imageLink} = req.body
  db.query(`UPDATE blog SET
  Title = "${Title}",
  subject = "${subject}",
  content = "${content}",
  imageLink = "${imageLink}",
  VideoID = "${VideoID}" 
  WHERE
  blogID = "${req.params.id}"`,
   function (error, results, fields) {
     if (error) throw error;
     return res.status(200).send(results);
 });
}); 

//**GET SUBMISSIONS**//
router.get('/contact_form/entries', jwtVerify, async (req, res, next) => {
    db.query(
        "SELECT * FROM submissions",
        function (error, results, fields) {
          if (error) throw error;
          return res.status(200).send(results);
        }
      );
  });

//**POST SUBMISSIONS**//
router.post('/contact_form/entries', async (req, res, next) => {
  db.query(
    "INSERT INTO submissions (name,DiscordH, Email, Message,CreationDate) VALUES (?, ?, ?, ?,?)",
    [req.body.name, req.body.DiscordH,req.body.Email, req.body.Message, req.body.CreationDate],
    function (error, results, fields) {
      if (error) throw error;
      return res.status(201).send(results);
    }
  );
});

//**DELETE SUBMISSIONS**//
router.delete('/contact_form/entries/:id', async (req, res, next) => {
  db.query(`DELETE FROM submissions WHERE SubmissionID=${req.params.id}`, function (error, results) {
    if (error) throw error
    return res.status(200).send(results)
})
});



//**POST USERS**//
router.post('/users', (req, res, next) => {
  let password = req.body.password;
  createHash(password).then(hash => {
      req.body.password = hash;
      return req.body;            
  }).then(async (newUser) => {
      const {Username, Email, password, name, CreationDate} = newUser;
      try {
          db.query(
              `INSERT INTO users (Username, Email, password, name,CreationDate) 
              VALUES ("${Username}", "${Email}", "${password}", "${name}", "${CreationDate}")`,
              function (error, results, fields){
                  if (error) throw error;
                  delete newUser.password;
                  return res.status(201).json(newUser);
              }
          );
      } catch (err) {
          console.error(err);
          return next(err);
      };
  });    
});


//**Patch USERS**//
router.patch("/users/:id", jwtVerify, async (req, res, next) => {
  const {Username, name, password, Email} = req.body
  db.query(`UPDATE users SET
  Username = "${Username}",
  name = "${name}",
  password = "${password}",
  Email = "${Email}" 
  WHERE
  userID = "${req.params.id}"`,
   function (error, results, fields) {
     if (error) throw error;
     return res.status(200).send(results);
 });
}); 

  
//**POST AUTH**//
router.post('/auth', async (req, res) => {
  db.query(
    `SELECT * FROM users WHERE Email = '${req.body.email}'`,
    function (error, results, fields) {
        if (error) throw error;
        const user = results[0];
        //In the event the email or password does not match: 
        if (!user) {
            return res.status(401).json({message: "incorrect credentials provided"});
        };  
        let password = req.body.password;
        let storedHash = user.password;
        verifyHash(password, storedHash).then(valid => {
            if (!valid) {
                return res.status(401).json({message: "incorrect credentials provided"});
            };
            //Upon successful login: 
            const userEmail = req.body.email;
            const token = jwt.sign({userEmail}, process.env.JWT_SECRET, {expiresIn: "2h"});      
            return res.json({token});
        });
    }
);
});


//** Get userlist **/
router.get('/users', jwtVerify, async (req, res, next) => {
  db.query(
      "SELECT * FROM users",
      function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
      }
    );
});

//**DELETE User**//
router.delete('/users/:id', async (req, res, next) => {
  db.query(`DELETE FROM users WHERE userID=${req.params.id}`, function (error, results) {
    if (error) throw error
    return res.status(200).send(results)
})
});





export default router
